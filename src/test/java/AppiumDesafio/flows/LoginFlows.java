package AppiumDesafio.flows;

import AppiumDesafio.pages.LoginPage;

public class LoginFlows {

    LoginPage loginPage;

    public LoginFlows(){
        loginPage = new LoginPage();
    }

    //Flows
    public void efetuarLogin(String username, String password) throws InterruptedException {


        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmFechar();
        loginPage.clicarEmEntrar();
        loginPage.preencherEmail(username);
        loginPage.preencherSenha(password);
        loginPage.clicarEmEntrar1();
        loginPage.clicarEmContinuar();


    }


}
