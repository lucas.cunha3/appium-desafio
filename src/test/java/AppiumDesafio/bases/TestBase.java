package AppiumDesafio.bases;

import AppiumDesafio.GlobalParameters;
import AppiumDesafio.utils.Relatorio;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

import static AppiumDesafio.GlobalParameters.*;


public class TestBase {

    public static AppiumDriver<MobileElement> driver;

    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }

    public static void inicializaDriver(boolean deviceFarm, boolean deviceReal)throws IOException {
        DesiredCapabilities caps = new DesiredCapabilities();

        if(deviceFarm){
            // Set your access credentials
            caps.setCapability("browserstack.user", AccessKeyBrowserStack);
            caps.setCapability("browserstack.key", UsernameBrowserStack);

            // Set URL of the application under test
            caps.setCapability("app", AppAndroidUploadBrowserStack);
            // Specify device and os_version for testing
            caps.setCapability("device", AndroidDeviceBrowserStack);
            caps.setCapability("os_version", AndroidOSVersionBrowserStack);

            // Set other BrowserStack capabilities
            caps.setCapability("project", AndroidProjectBrowserStack);
            caps.setCapability("build", BuildNumberBrowserStack);
            caps.setCapability("name", AndroidNameBrowserStack);


            // Initialise the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above

            driver = new AndroidDriver<MobileElement>(new URL(BrowserStackServer), caps);

        }
        else {
            if(deviceReal) {

                caps.setCapability("platformName", AndroidPlatformName);
                caps.setCapability("deviceName", AndroidRealDeviceName);
                caps.setCapability("appPackage", AndroidAppActivity);
                caps.setCapability("appActivity", AndroidAppPackage);

                driver = new AndroidDriver<MobileElement>(new URL(AppiumServer), caps);
            }
                else {

                caps.setCapability("platformName", AndroidPlatformName);
                caps.setCapability("deviceName", AndroidDeviceName);
                caps.setCapability("automationName", AndroidAutomationName);
                caps.setCapability("app",AndroidAppPath);
                driver = new AndroidDriver<MobileElement>(new URL(AppiumServer), caps);


            }

        }
    }


    @BeforeMethod(alwaysRun=true)
    public void beforeTest(Method method) throws Exception {
        inicializaDriver(isDeviceFarm, isRealDevice); //informar se sera realizado na deviceFarm ou dispositivo local (pc ou real)
        Relatorio.addTest(method.getName(), method.getDeclaringClass().getSimpleName());
    }

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        new GlobalParameters();
        Relatorio.iniciaRelatorio();
    }


    @AfterMethod(alwaysRun=true)
    public void afterTest(ITestResult result){
        Relatorio.adicionaResultadoTeste(result);
        driver.quit();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite(){
        Relatorio.geraRelatorio();
    }






}
