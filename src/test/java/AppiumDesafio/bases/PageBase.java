package AppiumDesafio.bases;

import AppiumDesafio.utils.Relatorio;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.WebDriverWait;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.apache.commons.lang3.time.StopWatch;

import java.time.Duration;
import java.util.HashMap;

import static org.aspectj.lang.reflect.DeclareAnnotation.Kind.Method;


public class PageBase {


    //Variaveis globals
    protected AppiumDriver driver = null;
    protected WebDriverWait wait = null;

    //Construtor
    public PageBase(){
        driver = TestBase.getDriver();
        wait = new WebDriverWait (driver, 10);
    }



    protected void click(MobileElement element) {
        WebDriverException possibleWebDriverException = null;
        StopWatch timeOut = new StopWatch();
        timeOut.start();
        while (timeOut.getTime() <= 20) {
            try {
                element.click();
                Relatorio.addTestInfo(3, "");
                timeOut.stop();
                return;
            } catch (StaleElementReferenceException e) {
                continue;
            } catch (WebDriverException e) {
                possibleWebDriverException = e;
                if (e.getMessage().contains("Other element would receive the click")) {
                    continue;
                }
                throw e;
            }
        }
        try {
            throw new Exception(possibleWebDriverException);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void sendKeys(MobileElement element, String text) {
        element.sendKeys(text);
        Relatorio.addTestInfo(3, "PARAMETER: " + text);
    }


    protected String getText(MobileElement element) {
        String text = element.getText();
        Relatorio.addTestInfo(3, "RETURN: " + text);
        return text;
    }

    protected void waitForElement(MobileElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForElementToBeClickeable(MobileElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }








}