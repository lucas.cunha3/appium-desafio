package AppiumDesafio.utils;

import AppiumDesafio.bases.TestBase;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public static String getMethodNameByLevel(int level){
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        return stackTrace[level].getMethodName();
    }
    public static String getScreenshot(String name, String path){
        File scrFile = ((TakesScreenshot)TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
        String filePathAndName = path + "/" +name + ".png";
        try {
            FileUtils.copyFile(scrFile, new File(filePathAndName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filePathAndName;
    }
    public static void createFolderReport(String folderName){
        File dir = new File(Utils.returnPathProject() + "\\target\\reports\\" + folderName);
        dir.mkdir();
    }
    public static String getAllStackTrace(ITestResult result){
        String allStackTrace = "";
        for(StackTraceElement stackTrace : result.getThrowable().getStackTrace()){
            allStackTrace = allStackTrace + "<br>" + stackTrace.toString();
        }
        return allStackTrace;
    }
    public static String getNowDate(String mask){
        DateFormat dateFormat = new SimpleDateFormat(mask);
        Date date = new Date();
        return dateFormat.format(date);
    }
    public static String getFileContent(String filePath) {
        BufferedReader br=null;
        StringBuilder sb=null;
        try {
            br = new BufferedReader(new FileReader(filePath));
            sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
        }  catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    public static void setContextToWebview(){
        Set<String> availableContexts = TestBase.getDriver().getContextHandles();
        availableContexts.stream()
                .filter(context -> context.toLowerCase().contains("webview"))
                .forEach(newcontext -> TestBase.getDriver().context(newcontext));
    }
    public String getDeviceDate(){
        return TestBase.getDriver().getDeviceTime();
    }


    public static String returnPathProject(){
        if(true){
            return System.getProperty("user.dir") + "\\";
        }
        else{
            return "";
        }
    }

    //Método para leitura do arquivo .csv
    public static Iterator<Object[]> csvProvider(String csvNamePath) {
        String line = "";
        String cvsSplitBy = ";";
        List<Object[]> testCases = new ArrayList<>();
        String[] data = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }


}
