package AppiumDesafio.utils;

import com.aventstack.extentreports.*;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.ITestResult;
import java.lang.reflect.Method;


import java.io.IOException;

public class Relatorio {

    public static ExtentReports EXTENT_REPORT = null; //instância do report
    public static ExtentHtmlReporter HTML_REPORTER = null; //tipo do report que será gerado (html)
    public static ExtentTest TEST; //objeto que adicionaremos informações sobre os testes
    public static String reportPath = "target/reports/"; //caminho de onde o arquivo do relatório será salvo


    static String reportName = "templateAppium_" + Utils.getNowDate("yyyy-MM-dd_HH-mm-ss");
    static String fullScreenShotPath = reportPath + "/"+ reportName;

    public static String fileName = reportName+".html"; //nome do relatório

    private static String testNameMemory = null;
    static Method method;

    static String fullReportFilePath = reportPath + "/"+ reportName +"/" + fileName;
    private static boolean getScreenshotForEachStep = false;

    public static void iniciaRelatorio() {
        EXTENT_REPORT = new ExtentReports();
        HTML_REPORTER = new ExtentHtmlReporter(fullReportFilePath);
        EXTENT_REPORT.attachReporter(HTML_REPORTER);
    }

    public static void addTest(String testName, String testCategory){
        testNameMemory = testName;
        TEST = EXTENT_REPORT.createTest(testName).assignCategory(testCategory.replace("Tests",""));
    }




    public static void addTestInfo(int methodLevel, String text){
        String methodName = Utils.getMethodNameByLevel(methodLevel);
        String testName = testNameMemory;
        if(getScreenshotForEachStep) {
            TEST.log(Status.PASS, methodName + " || " + text, getScreenShotMedia(testName+"_"+methodName+"_"+Utils.getNowDate("yyyy-MM-dd_HH-mm-ss-SSS")));
        }else {
            TEST.log(Status.PASS, methodName + " || " + text);
        }
    }


    public static void criaTesteRelatorio(String testName) {

        TEST = EXTENT_REPORT.createTest(testName);
    }

    public static void adicionaResultadoTeste(ITestResult result) {
        switch (result.getStatus())
        {
            case ITestResult.FAILURE:
                TEST.log(Status.FAIL, "Test Result: " + Status.FAIL + "<pre>" + "Message: " + result.getThrowable().toString() + "</pre>" + "<pre>" + "Stack Trace: " + Utils.getAllStackTrace(result) + "</pre>", getScreenShotMedia(testNameMemory));
                break;
            case ITestResult.SKIP:
                TEST.log(Status.SKIP, "Test Result: " + Status.SKIP + "<pre>" + "Message: " + result.getThrowable().toString() + "</pre>" + "<pre>" + "Stack Trace: " + Utils.getAllStackTrace(result) + "</pre>", getScreenShotMedia(testNameMemory));
                break;
            default:
                TEST.log(Status.PASS, "Test Result: " + Status.PASS, getScreenShotMedia(testNameMemory));
                break;
        }
    }

    public static MediaEntityModelProvider getScreenShotMedia(String testName){
        String screenshotPath = Utils.getScreenshot(testName, fullScreenShotPath);
        MediaEntityModelProvider media = null;
        try {
            media = MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath.replace(fullScreenShotPath,".")).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return media;
    }

    public static void geraRelatorio(){
        EXTENT_REPORT.flush();
    }

    public static void gravaInformacaoRelatorio(String mensagem){
        TEST.log(Status.INFO, mensagem);
    }


}
