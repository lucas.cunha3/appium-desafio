package AppiumDesafio.pages;

import AppiumDesafio.bases.PageBase;
import AppiumDesafio.bases.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PersonalLibraryPage extends PageBase {

    PersonalLibraryPage personalLibraryPage;

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public PersonalLibraryPage(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        wait = new WebDriverWait (driver, 60);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Fechar']")
    private MobileElement fecharButton;

    public void clicarEmFechar() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        click(fecharButton);
        Thread.sleep(1000);
    }



    @AndroidFindBy(xpath = "//android.widget.Button[5]")
    private MobileElement perfilButton;

    public void clicarNoIconePerfil() throws InterruptedException {
        click(perfilButton);
        Thread.sleep(1000);
    }




    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup[2]//android.widget.TextView)[2]")
    private MobileElement moreOptions2Button;

    public void clicarEmMaisOpcoes1() throws InterruptedException {
        Thread.sleep(2000);
        click(moreOptions2Button);
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='HQs (Quadrinhos)']")
    private MobileElement hqButton;

    public void clicarEmMinhasHQs() throws InterruptedException {
        click(hqButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='HQs']")
    private MobileElement textoHQ;

    public String receberTextoHQ() throws InterruptedException {
        return getText(textoHQ);
    }



    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Revistas']")
    private MobileElement revistasButton;

    public void clicarEmMinhasRevistas() throws InterruptedException {
        click(revistasButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Revistas']")
    private MobileElement textoRevistas;

    public String receberTextoRevistas() throws InterruptedException {
        return getText(textoRevistas);
    }



    @AndroidFindBy(xpath = "//android.widget.TextView[@text='PÁGINAS LIDAS']")
    private MobileElement textoPaginasLidas;

    public String receberTextoPaginasLidas() throws InterruptedException {
        Thread.sleep(1000);
        return getText(textoPaginasLidas);
    }



    @AndroidFindBy(xpath = "//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup//android.widget.ImageView")
    private MobileElement secondBookImage;

    public void clicarNoPrimeiroLivroDoPerfil() throws InterruptedException {
        click(secondBookImage);

    }


    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'TODOS')]")
    private MobileElement todosText;


    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'LIDOS')]")
    private MobileElement lidosText;



    @AndroidFindBy(xpath = "//android.widget.HorizontalScrollView//android.view.ViewGroup//android.view.ViewGroup[2]")
    private MobileElement readBooksButton;

    public void clicarNosLivrosLidos() throws InterruptedException {
        waitForElement(todosText);
        click(readBooksButton);
        waitForElement(lidosText);
    }

}
