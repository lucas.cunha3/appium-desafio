package AppiumDesafio.pages;

import AppiumDesafio.bases.PageBase;
import AppiumDesafio.bases.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginPage extends PageBase {

    LoginPage loginPage;

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public LoginPage(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        wait = new WebDriverWait (driver, 60);
    }


    @AndroidFindBy(xpath = "//*[@text='Próximo']")
    private MobileElement proximoButton;

    public void clicarEmProximo() throws InterruptedException {
        waitForElement(proximoButton);
        click(proximoButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Fechar']")
    private MobileElement fecharButton;

    public void clicarEmFechar() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        click(fecharButton);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text=' Entrar ']")
    private MobileElement entrarButton;

    public void clicarEmEntrar() throws InterruptedException {
        click(entrarButton);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Email']")
    private MobileElement emailButton;

    public void preencherEmail(String email) throws InterruptedException {click(emailButton);sendKeys(emailButton, email);}

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Senha']")
    private MobileElement senhaButton;

    public void preencherSenha(String password) throws InterruptedException {
        click(senhaButton);
        sendKeys(senhaButton, password);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='ENTRAR']")
    private MobileElement entrarButton2;

    public void clicarEmEntrar1() throws InterruptedException {
        click(entrarButton2);
        waitForElementToBeClickeable(entrarButton2);
        click(entrarButton2);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Email ou senha inválidos']")
    private MobileElement message;

    public String receberTextoDeErro()
    {
        return getText(message);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Continuar']")
    private MobileElement continuarButton;

    public void clicarEmContinuar() throws InterruptedException {
        waitForElementToBeClickeable(continuarButton);
        click(continuarButton);
    }

    @AndroidFindBy(xpath = "//android.widget.Button[5]")
    private MobileElement perfilButton;

    public void clicarNoIconePerfil() throws InterruptedException {
        click(perfilButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Base2Test']")
    private MobileElement tituloLoginPerfil;

    public String receberNomePerfilLogado() throws InterruptedException {
        return getText(tituloLoginPerfil);
    }


    public String existeTextoProximo() throws InterruptedException {
        return getText(proximoButton);
    }


}
