package AppiumDesafio.pages;

import AppiumDesafio.bases.PageBase;
import AppiumDesafio.bases.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class ProfilePage extends PageBase {

    ProfilePage profilePage;

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public ProfilePage(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        wait = new WebDriverWait (driver, 60);
    }




    @AndroidFindBy(xpath = "//android.widget.Button[5]")
    private MobileElement perfilButton;

    public void clicarNoIconePerfil() throws InterruptedException {
        click(perfilButton);
        Thread.sleep(1000);
    }



    @AndroidFindBy(xpath = "//android.widget.Button[3]")
    private MobileElement searchButton;

    public void clicarNoIconeDePesquisa() throws InterruptedException {
        click(searchButton);
    }

    @AndroidFindBy(className = "android.widget.EditText")
    private MobileElement filterButton;

    public void clicarParaDigitar(String text) throws InterruptedException {
        click(filterButton);sendKeys(filterButton, text);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Leitores']")
    private MobileElement readersButton;

    public void clicarEmLeitores() throws InterruptedException {
        click(readersButton);
    }




    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Seguindo']")
    private MobileElement seguindoButton;

    public void clicarEmUsuariosQueEstouSeguindo() throws InterruptedException {
        click(seguindoButton);
    }

    @AndroidFindBy(xpath = "//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup")
    private MobileElement firstFollowing;

    public void clicarNoPrimeiroUsuarioSeguindo() throws InterruptedException {
        click(firstFollowing);
    }

    @AndroidFindBy(xpath = "(//android.widget.TextView[@text='Seguindo'])[2]")
    private MobileElement seguindoButton2;

    public void clicarParaPararDeSeguir() throws InterruptedException {
        click(seguindoButton2);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Seguir']")
    private MobileElement seguirTexto;

    public String receberTextoSeguir() throws InterruptedException {
        return getText(seguirTexto);
    }



    @AndroidFindBy(xpath = "//android.widget.Button[4]")
    private MobileElement meusLivrosButton;

    public void clicarEmMeusLivros() throws InterruptedException {
        click(meusLivrosButton);
    }



    @AndroidFindBy(xpath = "//android.widget.Button[2]")
    private MobileElement newReleasesButton;

    public void clicarEmLancamentos() throws InterruptedException {
        click(newReleasesButton);
        Thread.sleep(1500);
    }

    @AndroidFindBy(xpath = "//android.view.ViewGroup[3]//android.view.ViewGroup")
    private MobileElement cortesyButton;

    public void clicarEmCortesias() throws InterruptedException {
        click(cortesyButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cortesias']")
    private MobileElement textoCortesias;

    public String receberTextoCortesias() throws InterruptedException {
        Thread.sleep(1000);
        return getText(textoCortesias);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Lançamentos']")
    private MobileElement textoLancamentos;

    public String receberTextoLancamentos() throws InterruptedException {
        Thread.sleep(1000);
        return getText(textoLancamentos);
    }



    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[15]")
    private MobileElement pagesOnTheGoalText;

    public String receberPaginasNaMeta() throws InterruptedException {
        Thread.sleep(2000);
        return getText(pagesOnTheGoalText);
    }



    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")
    private MobileElement firstUserResearch;

    public void clicarNoPrimeiroUsuarioDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(firstUserResearch);
        Thread.sleep(3000);
    }



    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Mensagem...']")
    private MobileElement insertMessageField;

    public void clicarParaDigitarDM(String text) throws InterruptedException {
        click(insertMessageField);sendKeys(insertMessageField, text);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[3]")
    private MobileElement messageIcon;

    public void clicarNoIconeMensagem() throws InterruptedException {
        click(messageIcon);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Enviar']")
    private MobileElement sendButton;

    public void clicarEmEnviarDM() throws InterruptedException {
        click(sendButton);
    }

    @AndroidFindBy(xpath = "(//android.widget.TextView)[3]")
    private MobileElement sentMessage;

    public String receberUltimaDMEnviada() throws InterruptedException {
        Thread.sleep(2000);
        return getText(sentMessage);
    }


}
