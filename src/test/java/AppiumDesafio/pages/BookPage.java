package AppiumDesafio.pages;

import AppiumDesafio.bases.PageBase;
import AppiumDesafio.bases.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BookPage extends PageBase {

    BookPage bookPage;

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public BookPage(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        wait = new WebDriverWait (driver, 60);
    }



    @AndroidFindBy(xpath = "//android.widget.Button[5]")
    private MobileElement perfilButton;

    public void clicarNoIconePerfil() throws InterruptedException {
        click(perfilButton);
        Thread.sleep(1000);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'valia')]")
    private MobileElement parcialTextAvali;

    @AndroidFindBy(xpath = "(//android.widget.TextView)[2]")
    private MobileElement plusButton;

    public void clicarIconeMais() throws InterruptedException {
        click(parcialTextAvali);
        click(plusButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Excluir da estante']")
    private MobileElement deleteButton2;

    public void clicarEmRetirarDaEstante() throws InterruptedException {
        click(deleteButton2);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Retirado da estante!']")
    private MobileElement confirmationText;

    public String receberConfirmacaoDaExclusao() throws InterruptedException {
        Thread.sleep(1000);
        return getText(confirmationText);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Já li']")
    private MobileElement alreadyReadButton;

    public void clicarEmJaLi() throws InterruptedException {
        click(alreadyReadButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Quero ler']")
    private MobileElement wantToReadButton;

    public void clicarEmQueroLer() throws InterruptedException {
        click(wantToReadButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Lendo']")
    private MobileElement readingButton;

    public void clicarEmLendo() throws InterruptedException {
        click(readingButton);
    }





    @AndroidFindBy(xpath = "//android.view.ViewGroup//android.widget.Button[5]")
    private MobileElement veryGoodButton;

    public void clicarEm4Estrelas() throws InterruptedException {
        Thread.sleep(1000);
        click(veryGoodButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='MUITO BOM']")
    private MobileElement veryGoodText;

    public String receberTextoMuitoBom() throws InterruptedException {
        Thread.sleep(2000);
        return getText(veryGoodText);
    }

    @AndroidFindBy(xpath = "//android.view.ViewGroup//android.widget.Button[2]")
    private MobileElement badButton;

    public void clicarEm2Estrelas() throws InterruptedException {
        Thread.sleep(1000);
        click(badButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='RUIM']")
    private MobileElement badText;

    public String receberTextoRuim() throws InterruptedException {
        Thread.sleep(2000);
        return getText(badText);
    }




    @AndroidFindBy(xpath = "//android.widget.TextView[@text='MAIS OPÇÕES']")
    private MobileElement moreOptions3;

    public void clicarEmMaisOpcoes3() throws InterruptedException {
        click(moreOptions3);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Novo']")
    private MobileElement newButton;

    public void clicarEmNovo() throws InterruptedException {
        click(newButton);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[contains(@text,'lidas')]")
    private MobileElement pageField;

    public void alterarNumeroDePaginasLidas(String text) throws InterruptedException {
        click(pageField);sendKeys(pageField, text);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Páginas']")
    private MobileElement pageCheckbox;

    public void clicarNoCheckboxPaginas() throws InterruptedException {
        click(pageCheckbox);click(pageCheckbox);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Salvar']")
    private MobileElement saveButton;

    public void clicarEmSalvar() throws InterruptedException {
        waitForElement(saveButton);
        click(saveButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'sucesso')]")
    private MobileElement successMessage;

    public String receberMensagemExibidaDeSucesso() throws InterruptedException {
        Thread.sleep(1000);
        return getText(successMessage);
    }


}
