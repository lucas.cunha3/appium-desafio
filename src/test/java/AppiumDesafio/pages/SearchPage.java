package AppiumDesafio.pages;

import AppiumDesafio.bases.PageBase;
import AppiumDesafio.bases.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

public class SearchPage extends PageBase {

    SearchPage searchPage;

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public SearchPage(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        wait = new WebDriverWait (driver, 60);
    }





    @AndroidFindBy(xpath = "//android.widget.Button[5]")
    private MobileElement perfilButton;

    public void clicarNoIconePerfil() throws InterruptedException {
        click(perfilButton);
        Thread.sleep(1000);
    }




    @AndroidFindBy(className = "android.widget.EditText")
    private MobileElement filterButton;

    public void clicarParaDigitar(String text) throws InterruptedException {
        click(filterButton);sendKeys(filterButton, text);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Leitores']")
    private MobileElement readersButton;

    public void clicarEmLeitores() throws InterruptedException {
        click(readersButton);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Seguir']")
    private MobileElement seguirButton;

    public void clicarEmSeguir() throws InterruptedException {
        click(seguirButton);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[11]")
    private MobileElement textoRecebido;

    public String receberTextoQueSeguiuUsuario() throws InterruptedException {
        return getText(textoRecebido);
    }


    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")
    private MobileElement tittleOfFirstBook;

    public String receberTituloDoPrimeiroLivroNaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        return getText(tittleOfFirstBook);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[8]")
    private MobileElement tittleOfTheAuthorOfTheFirstBook;

    public String receberTituloDoAutorDoPrimeiroLivroRecebido() throws InterruptedException {
        Thread.sleep(1000);
        return getText(tittleOfTheAuthorOfTheFirstBook);
    }


    @AndroidFindBy(xpath = "(//android.widget.TextView)[2]")
    private MobileElement plusButton;

    public void clicarIconeMais() throws InterruptedException {
        click(plusButton);
    }



    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Já li']")
    private MobileElement alreadyReadButton;

    public void clicarEmJaLi() throws InterruptedException {
        click(alreadyReadButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Quero ler']")
    private MobileElement wantToReadButton;

    public void clicarEmQueroLer() throws InterruptedException {
        click(wantToReadButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Lendo']")
    private MobileElement readingButton;

    public void clicarEmLendo() throws InterruptedException {
        click(readingButton);
    }




    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Quantas estrelas?']")
    private MobileElement howManyStarsQuestionText;

    public String receberTextoPerguntandoEstrelas() throws InterruptedException {
        Thread.sleep(2000);
        return getText(howManyStarsQuestionText);
    }



    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")
    private MobileElement firstBookButton;

    public void clicarNoPrimeiroLivroDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(firstBookButton);
        Thread.sleep(3000);
    }


    @AndroidFindBy(xpath = "(//android.widget.TextView[2])[1]")
    private MobileElement askingOpinionText;

    public String receberTextoPerguntandoResenha() throws InterruptedException {
        Thread.sleep(2000);
        return getText(askingOpinionText);
    }


    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[14]")
    private MobileElement secondBookOfTheResearch;

    public void clicarNoSegundoLivroDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(secondBookOfTheResearch);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Meta de Leitura']")
    private MobileElement metaDeLeituraText;

    public String receberTextoMetaDeLeitura() throws InterruptedException {
        Thread.sleep(2000);
        return getText(metaDeLeituraText);
    }



    @AndroidFindBy(xpath = "//android.widget.TextView[@text='2023']")
    private MobileElement wantToRead2023Button;

    public void clicarEm2023() throws InterruptedException {
        Thread.sleep(1000);
        click(wantToRead2023Button);
        Thread.sleep(1000);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FECHAR']")
    private MobileElement xButton;

    public void clicarNoX() throws InterruptedException {
        click(xButton);
    }

    @AndroidFindBy(xpath = "//android.widget.Button[3]//android.view.ViewGroup")
    private MobileElement searchButton1;

    public void clicarNoIconeDePesquisa1() throws InterruptedException {
        click(searchButton1);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[9]")
    private MobileElement secondUserResearch;

    public void clicarNoSegundoUsuarioDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(secondUserResearch);
        Thread.sleep(3000);
    }

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]")
    private MobileElement moreOptionsButton;

    public void clicarEmMaisOpcoes2() throws InterruptedException {
        click(moreOptionsButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Amigos?')]")
    private MobileElement addUser;

    public void clicarEmAdicionarUsuario() throws InterruptedException {
        click(addUser);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Sim')]")
    private MobileElement yesButton;

    public void clicarEmSim() throws InterruptedException {
        click(yesButton);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Solicitação enviada.')]")
    private MobileElement sentRequestText;

    public String receberTextoSolicitacaoEnviada() throws InterruptedException {
        Thread.sleep(2000);
        return getText(sentRequestText);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")
    private MobileElement firstUserResearch;

    public void clicarNoPrimeiroUsuarioDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(firstUserResearch);
        Thread.sleep(3000);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[11]")
    private MobileElement thirdUserResearch;

    public void clicarNoTerceiroUsuarioDaPesquisa() throws InterruptedException {
        Thread.sleep(1000);
        click(thirdUserResearch);
        Thread.sleep(3000);
    }







    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Resenha']")
    private MobileElement reviewButton;

    public void clicarEmRevisao() throws InterruptedException {
        click(reviewButton);
    }

    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.widget.TextView[1])[11]")
    private MobileElement messageText;

    public String receberMensagemExibida() throws InterruptedException {
        Thread.sleep(2000);
        return getText(messageText);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='MAIS OPÇÕES']")
    private MobileElement moreOptions3;

    public void clicarEmMaisOpcoes3() throws InterruptedException {
        click(moreOptions3);
    }



    @AndroidFindBy(xpath = "(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[3]")
    private MobileElement copyIcon;

    public void clicarNoIconeDeCopiar() throws InterruptedException {
        Thread.sleep(2000);
        click(copyIcon);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'perfil copiado')]")
    private MobileElement alertMessage;

    public String retornarMensagemDePerfilCopiado() throws InterruptedException {
        return getText(alertMessage);
    }



}
