package AppiumDesafio.tests;

import AppiumDesafio.bases.TestBase;
import AppiumDesafio.flows.LoginFlows;
import AppiumDesafio.pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;


public class BookTests extends TestBase {
    LoginFlows loginFlows;
    SearchPage searchPage;
    ProfilePage profilePage;
    PersonalLibraryPage personalLibraryPage;
    MainPage mainPage;
    BookPage bookPage;


    public final String livroParaColocarComoLidoOuLendo = "Amizade";
    public final String livroParaColocarComoQueroLerEMuitoBom = "Americ";
    public final String livroParaColocarComoRuim = "Cole";
    public final String livroParaResenhar = "Sorvete";
    public final String livroParaInformarPaginas = "Passeio";

    String emailCorreto = "lucas.cunha@base2.com.br";
    String senhaCorreta = "0914x8Cb";


    @Test
    public void CT05_adicionarUmLivroNaMetaDeLeitura() throws InterruptedException {

        //Instancia de Pagina
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        String paginasNaMetaAntiga = profilePage.receberPaginasNaMeta();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoRuim);
        searchPage.clicarNoPrimeiroLivroDaPesquisa();
        searchPage.clicarIconeMais();
        searchPage.clicarEmQueroLer();
        searchPage.clicarEm2023();
        searchPage.clicarNoX();
        searchPage.clicarNoIconeDePesquisa1();
        searchPage.clicarNoIconePerfil();
        String paginasNaMetaNova = profilePage.receberPaginasNaMeta();
        Assert.assertNotEquals(paginasNaMetaNova, paginasNaMetaAntiga);


    }

    @Test
    public void CT09_adicionarUmLivroComoLido() throws InterruptedException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String resultadoEsperado = "Quantas estrelas?";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoLidoOuLendo);
        searchPage.clicarNoPrimeiroLivroDaPesquisa();
        searchPage.clicarIconeMais();
        searchPage.clicarEmQueroLer();
        searchPage.clicarEmJaLi();
        String resultadoRecebido = searchPage.receberTextoPerguntandoEstrelas();
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);



    }


    @Test
    public void CT11_adicionarUmLivroComoQueroLer() throws InterruptedException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String resultadoEsperado = "Meta de Leitura";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoQueroLerEMuitoBom);
        searchPage.clicarNoPrimeiroLivroDaPesquisa();
        searchPage.clicarIconeMais();
        searchPage.clicarEmJaLi();
        searchPage.clicarEmQueroLer();
        String resultadoRecebido = searchPage.receberTextoMetaDeLeitura();
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);

    }

    @Test
    public void CT12_validarQueUmLivroLendoNaoEPossivelSerResenhado() throws InterruptedException, MalformedURLException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String resultadoEsperado = "Para fazer a resenha, seu livro precisa estar marcado como LIDO.";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaResenhar);
        searchPage.clicarNoPrimeiroLivroDaPesquisa();
        searchPage.clicarIconeMais();
        searchPage.clicarEmQueroLer();
        searchPage.clicarEmLendo();
        searchPage.clicarEmMaisOpcoes3();
        searchPage.clicarEmRevisao();
        String resultadoRecebido = searchPage.receberMensagemExibida();
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);

    }

    @Test
    public void CT16_acessarMeusLivros() throws InterruptedException {
        //Instancia de Paginas
        loginFlows = new LoginFlows();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        personalLibraryPage = new PersonalLibraryPage();

        //Parametros
        String textoEsperado = "PÁGINAS LIDAS";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarEmMeusLivros();
        String textoRecebido = personalLibraryPage.receberTextoPaginasLidas();
        Assert.assertEquals(textoRecebido, textoEsperado);

    }


    @Test
    public void CT21_avaliarUmLivroComoRuim() throws InterruptedException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        bookPage = new BookPage();

        //Parametros
        String resultadoEsperado = "RUIM";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoRuim);
        searchPage.clicarNoSegundoLivroDaPesquisa();
        bookPage.clicarIconeMais();
        bookPage.clicarEmQueroLer();
        bookPage.clicarEmJaLi();
        bookPage.clicarEm2Estrelas();
        String resultadoRecebido = bookPage.receberTextoRuim();
        System.out.println(resultadoRecebido);
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);

    }


    @Test
    public void CT20_avaliarUmLivroComoMuitoBom() throws InterruptedException {
        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        bookPage = new BookPage();

        //Parametros
        String resultadoEsperado = "MUITO BOM";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoQueroLerEMuitoBom);
        //Thread.sleep(2000);
        searchPage.clicarNoSegundoLivroDaPesquisa();
        //Thread.sleep(4000);
        bookPage.clicarIconeMais();
        bookPage.clicarEmQueroLer();
        bookPage.clicarEmJaLi();
        bookPage.clicarEm4Estrelas();
        String resultadoRecebido = bookPage.receberTextoMuitoBom();
        System.out.println(resultadoRecebido);
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);

    }

    @Test
    public void CT19_adicionarUmHistoricoDeLeitura() throws InterruptedException, MalformedURLException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        bookPage = new BookPage();

        //Parametros
        String resultadoEsperado = "Sua anotação foi salva com sucesso!";
        final String numeroPaginas = "13";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaInformarPaginas);
        //Thread.sleep(2000);
        searchPage.clicarNoSegundoLivroDaPesquisa();
        //Thread.sleep(4000);
        bookPage.clicarIconeMais();
        bookPage.clicarEmLendo();
        bookPage.clicarEmMaisOpcoes3();
        bookPage.clicarEmNovo();
        bookPage.alterarNumeroDePaginasLidas(numeroPaginas);
        bookPage.clicarNoCheckboxPaginas();
        bookPage.clicarEmSalvar();
        String resultadoRecebido = bookPage.receberMensagemExibidaDeSucesso();
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);


    }

    @Test
    public void CT10_adicionarUmLivroComoLendo() throws InterruptedException {

        //Instancia de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String resultadoEsperado = "Não deixe de voltar e dizer o que achou da leitura.";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaColocarComoLidoOuLendo);
        searchPage.clicarNoSegundoLivroDaPesquisa();
        searchPage.clicarIconeMais();
        searchPage.clicarEmQueroLer();
        searchPage.clicarEmLendo();
        String resultadoRecebido = searchPage.receberTextoPerguntandoResenha();
        Assert.assertEquals(resultadoEsperado, resultadoRecebido);



    }

    @Test
    public void CT22_excluirUmLivroDoMeuPerfil() throws InterruptedException {
        //Instancia de Paginas
        loginFlows = new LoginFlows();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        personalLibraryPage = new PersonalLibraryPage();
        bookPage = new BookPage();

        //Parametros
        String mensagemEsperada = "Retirado da estante!";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarEmMeusLivros();
        personalLibraryPage.clicarNosLivrosLidos();
        personalLibraryPage.clicarNoPrimeiroLivroDoPerfil();
        bookPage.clicarIconeMais();
        bookPage.clicarEmRetirarDaEstante();
        String mensagemRecebida = bookPage.receberConfirmacaoDaExclusao();
        Assert.assertEquals(mensagemEsperada,mensagemRecebida);


    }



}

