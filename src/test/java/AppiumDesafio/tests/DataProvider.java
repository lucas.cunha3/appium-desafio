package AppiumDesafio.tests;

import AppiumDesafio.bases.TestBase;
import AppiumDesafio.utils.Utils;

import java.util.Iterator;


public class DataProvider extends TestBase {

    @org.testng.annotations.DataProvider(name = "dataProjetoCSVProvider")
    public Iterator<Object[]> dataProjetoProvider() {
        return  Utils.csvProvider("src/test/java/dataProjeto.csv");
    }



}

