package AppiumDesafio.tests;

import AppiumDesafio.bases.TestBase;
import AppiumDesafio.flows.LoginFlows;
import AppiumDesafio.pages.*;
import com.sun.javafx.scene.control.behavior.ScrollBarBehavior;
import javafx.scene.input.ScrollEvent;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.touch.ScrollAction;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;

import static org.openqa.selenium.Keys.DOWN;


public class FriendshipTests extends TestBase {
    LoginFlows loginFlows;
    SearchPage searchPage;
    ProfilePage profilePage;
    MainPage mainPage;

    //Parametros
    public final String usuarioParaSeguir = "Carla A";
    String emailCorreto = "lucas.cunha@base2.com.br";
    String senhaCorreta = "0914x8Cb";


    @Test
    public void CT03_seguirUmUsuario() throws InterruptedException {

        //Instanciamento de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String textoEsperado = "Seguindo";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarEmLeitores();
        searchPage.clicarParaDigitar(usuarioParaSeguir);
        searchPage.clicarNoPrimeiroUsuarioDaPesquisa();
        searchPage.clicarEmSeguir();
        String statusRecebido = searchPage.receberTextoQueSeguiuUsuario();
        Assert.assertEquals(textoEsperado,statusRecebido);

    }

    @Test
    public void CT04_pararDeSeguirUsuario() throws InterruptedException {
        //Instanciamento de Paginas
        loginFlows = new LoginFlows();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String textoEsperado = "Seguir";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarEmUsuariosQueEstouSeguindo();
        profilePage.clicarNoPrimeiroUsuarioSeguindo();
        profilePage.clicarParaPararDeSeguir();
        String textoRecebido = profilePage.receberTextoSeguir();
        Assert.assertEquals(textoEsperado, textoRecebido);
    }

    @Test
    public void CT06_enviarMensagemParaUmUsuario() throws InterruptedException, MalformedURLException {

        //Instanciamento de Paginas
        loginFlows = new LoginFlows();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String mensagem = "Ola, tudo bem?";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        profilePage.clicarEmLeitores();
        profilePage.clicarParaDigitar(usuarioParaSeguir);
        profilePage.clicarNoPrimeiroUsuarioDaPesquisa();
        profilePage.clicarNoIconeMensagem();
        profilePage.clicarParaDigitarDM(mensagem);
        profilePage.clicarEmEnviarDM();
        String resultadoRecebido = profilePage.receberUltimaDMEnviada();
        Assert.assertEquals(mensagem,resultadoRecebido);

    }

    @Test
    public void CT17_enviarConviteDeAmizade() throws InterruptedException {
        //Instanciamento de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String mensagemEsperada = "Solicitação enviada.";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarEmLeitores();
        searchPage.clicarParaDigitar(usuarioParaSeguir);
        searchPage.clicarNoSegundoUsuarioDaPesquisa();
        searchPage.clicarEmMaisOpcoes2();
        searchPage.clicarEmAdicionarUsuario();
        searchPage.clicarEmSim();
        String mensagemRecebida = searchPage.receberTextoSolicitacaoEnviada();
        Assert.assertEquals(mensagemRecebida, mensagemEsperada);

    }

    @Test
    public void CT18_copiarInfoDoPerfil() throws InterruptedException, MalformedURLException {

        //Instanciamento de Paginas
        loginFlows = new LoginFlows();
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();

        //Parametros
        String mensagemEsperada = "Link do perfil copiado";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarEmLeitores();
        searchPage.clicarParaDigitar(usuarioParaSeguir);
        searchPage.clicarNoTerceiroUsuarioDaPesquisa();
        searchPage.clicarEmMaisOpcoes2();
        searchPage.clicarNoIconeDeCopiar();
        String mensagemRecebida = searchPage.retornarMensagemDePerfilCopiado();

        System.out.println(mensagemRecebida);

        Assert.assertEquals(mensagemRecebida, mensagemEsperada);


    }


}

