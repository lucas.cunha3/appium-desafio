package AppiumDesafio.tests;

import AppiumDesafio.bases.TestBase;
import AppiumDesafio.flows.LoginFlows;
import AppiumDesafio.pages.LoginPage;
import AppiumDesafio.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;


public class LoginTests extends TestBase {
    LoginFlows loginFlows;
    LoginPage loginPage;
    MainPage mainPage;


    String emailCorreto = "lucas.cunha@base2.com.br";
    String senhaCorreta = "0914x8Cb";


    @Test
    public void CT26_loginComSenhaInvalida() throws InterruptedException, MalformedURLException {

        //Instanciamento de Pagina
        loginPage = new LoginPage();

        //Parametros
        String email = "lucas.cunha@base2.com.br";
        String senha = "Testing2023";
        String mensagemEsperada = "Email ou senha inválidos";

        //Teste
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmFechar();
        loginPage.clicarEmEntrar();
        loginPage.preencherEmail(email);
        loginPage.preencherSenha(senha);
        loginPage.clicarEmEntrar1();
        String mensagemRecebida = loginPage.receberTextoDeErro();
        Assert.assertEquals(mensagemRecebida, mensagemEsperada);

    }

    @Test
    public void CT02_loginComSucesso() throws InterruptedException {

        //Instanciamento de Pagina
        loginFlows = new LoginFlows();
        loginPage = new LoginPage();
        mainPage = new MainPage();

        //Parametros
        String tituloEsperado = "Base2Test";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        String mensagemRecebida = loginPage.receberNomePerfilLogado();
        Assert.assertEquals(mensagemRecebida, tituloEsperado);

    }

    @Test
    public void CT14_loginComEmailInvalido() throws InterruptedException, MalformedURLException {

        //Instanciamento de Pagina
        loginPage = new LoginPage();

        //Parametros
        String email = "lucas.cunXha@base2.com.br";
        String senha = "Testing2022";
        String mensagemEsperada = "Email ou senha inválidos";

        //Teste
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmFechar();
        loginPage.clicarEmEntrar();
        loginPage.preencherEmail(email);
        loginPage.preencherSenha(senha);
        loginPage.clicarEmEntrar1();
        String mensagemRecebida = loginPage.receberTextoDeErro();
        Assert.assertEquals(mensagemRecebida, mensagemEsperada);

    }

    @Test(dataProvider = "dataProjetoCSVProvider", dataProviderClass = DataProvider.class)
    public void CT25_testeComDataDriven2(String[] projetoData) throws InterruptedException {

        //Instanciamento de Pagina
        loginPage = new LoginPage();

        //Parametros
        String email = projetoData[0];
        String senha = projetoData[1];
        String mensagemEsperada = "Email ou senha inválidos";

        //Teste
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmProximo();
        loginPage.clicarEmFechar();
        loginPage.clicarEmEntrar();
        loginPage.preencherEmail(email);
        loginPage.preencherSenha(senha);
        loginPage.clicarEmEntrar1();
        String mensagemRecebida = loginPage.receberTextoDeErro();
        Assert.assertEquals(mensagemRecebida, mensagemEsperada);

    }

    @Test
    public void CT01_verificarSeAppFoiAberto() throws InterruptedException {
        //Instanciamento de Pagina
        loginPage = new LoginPage();

        //Parametros
        String tituloEsperado = "Próximo";

        //Teste
        String textoNoBotao = loginPage.existeTextoProximo();
        Assert.assertEquals(textoNoBotao, tituloEsperado);


    }

}

