package AppiumDesafio.tests;

import AppiumDesafio.bases.TestBase;
import AppiumDesafio.flows.LoginFlows;
import AppiumDesafio.pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;


public class SearchTests extends TestBase {
    LoginFlows loginFlows;
    SearchPage searchPage;
    ProfilePage profilePage;
    MainPage mainPage;

    //Parametros
    String emailCorreto = "lucas.cunha@base2.com.br";
    String senhaCorreta = "0914x8Cb";


    @Test
    public void CT07_pesquisarUmAutor() throws InterruptedException {

        //Instanciamento de Pagina
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        loginFlows = new LoginFlows();

        //Parametros
        String autorParaPesquisar = "Suzanne Collins";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(autorParaPesquisar);
        String livroRecebido = searchPage.receberTituloDoAutorDoPrimeiroLivroRecebido();
        Assert.assertTrue(livroRecebido.contains(autorParaPesquisar));

    }

    @Test
    public void CT08_pesquisarUmLivro() throws InterruptedException {

        //Instanciamento de Pagina
        searchPage = new SearchPage();
        profilePage = new ProfilePage();
        mainPage = new MainPage();
        loginFlows = new LoginFlows();

        //Parametros
        String livroParaPesquisar = "O Mar de Monstros";

        //Teste
        loginFlows.efetuarLogin(emailCorreto,senhaCorreta);
        mainPage.clicarNoIconePerfil();
        profilePage.clicarNoIconeDePesquisa();
        searchPage.clicarParaDigitar(livroParaPesquisar);
        String livroRecebido = searchPage.receberTituloDoPrimeiroLivroNaPesquisa();
        Assert.assertTrue(livroRecebido.contains(livroParaPesquisar));
    }


}

